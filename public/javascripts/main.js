define(function(require, exports, module) {
  // Using jQuery for DOM, Backbone for MVC, Jade for template render
  var $ = require('jquery')
    , backbone = require('backbone')
    , jade = require('jade');

  var QuadraticEquationModel = backbone.Model.extend({
    // Set default attr safe
    defaults: {
      a: 0,
      b: 0,
      c: 0,
      roots: null
    },

    // , REST url
    url: '/quadratic-equation',

    solve: function() {
      this.save({ roots: null });
    }
  });

  var QuadraticEquationView = backbone.View.extend({
    events: {
      'click input[type=submit]': 'onSolve',
      'submit form': 'onSolve'
    },

    initialize: function() {
      this.model = this.options.model;

      // Eq update => update render
      this.model.on('change', this.render, this);
      
      // Eq solve error => set error
      this.model.on('error', this.onError, this);
    },

    // Display error
    onError: function(model, resp) {
      this.$('.error').text(resp.responseText);
    },

    // Handler user submit event
    onSolve: function(e) {
      // Prevent form submit
      e.preventDefault(); 
      
      // Set value to model
      this.model.set({
        a: this.$('#coefficient-a').val(),
        b: this.$('#coefficient-b').val(),
        c: this.$('#coefficient-c').val()
      });

      // Call sovle async
      this.model.solve();
    },

    // Create compile template (share among views)
    template: jade.compile(require('text!templates/quadratic-equation.jade')),

    render: function() {
      // Render view from template
      this.$el.html(this.template(this.model.toJSON()));
      return this;
    }
  });

  // Create equation view with eq
  var eq = new QuadraticEquationModel();
  var view = new QuadraticEquationView({ model: eq });
  //var view2 = new QuadraticEquationView({ model: eq });

  // Render it and append to body
  $(document.body).append(view.render().el);
  //$(document.body).append(view2.render().el);
});
