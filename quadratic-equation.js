var FindQuadraticEquationRoots = function(a, b, c) {
  var d = b*b - 4*a*c;

  // Delta < 0, no real root
  if (d < 0) {
    return [];
  }
  // Delta = 0, double root 
  if (d == 0) {
    return [(-b + Math.sqrt(d))/(2*a)];
  }
  // Delta > 0, 2 distinct roots
  return [(-b + Math.sqrt(d))/(2*a), (-b - Math.sqrt(d))/(2*a)];
}

var QuadraticEquation = function(a, b, c) {
  if (isNaN(a) || isNaN(b) || isNaN(c) || a == 0) {
    throw new Error('a, b, c must be number and a != 0');
  }

  this.a = a;
  this.b = b;
  this.c = c;
}

QuadraticEquation.prototype.solve = function() {
  return FindQuadraticEquationRoots(this.a, this.b, this.c);
}

exports.FindQuadraticEquationRoots = FindQuadraticEquationRoots;
exports.QuadraticEquation = QuadraticEquation;